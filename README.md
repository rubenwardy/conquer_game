# Conquer Game

[![pipeline status](https://gitlab.com/rubenwardy/conquer_game/badges/master/pipeline.svg)](https://gitlab.com/rubenwardy/conquer/commits/master)

Command your troops to victory.

Supports Minetest Game.

## License and Credits

Source: LGPLv2.1, Media: CC-BY-SA 3.0.

* conquer_humanlike.b3d from [Player API](https://github.com/minetest/minetest_game/tree/master/mods/player_api) by various contributors, CC BY-SA 3.0.
* conquer_unit_* skins created by GreenXenith, CC-BY-SA 3.0, modified by rubenwardy.
* conquer_arrow_*.png from [Simple Shooter](https://github.com/stujones11/shooter) by Stuart Jones, CC0 1.0.
* conquer_arrow.b3d from [Simple Shooter](https://github.com/stujones11/shooter) by Stuart Jones, CC-BY-SA 3.0.
* All other textures and models created by rubenwardy, CC-BY-SA 3.0.
