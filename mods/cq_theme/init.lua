
minetest.register_on_joinplayer(function(player)
	-- Set formspec prepend
	local formspec = [[
			bgcolor[#080808BB;true]
			listcolors[#00000069;#5A5A5A;#141318;#30434C;#FFF]
			background9[5,5;1,1;cq_theme_panel.png;true;10]
		]]

	player:set_formspec_prepend(formspec)
end)
