exclude_files = {
	"mods/deps",
}

read_globals = {
	"DIR_DELIM",
	"minetest", "core",
	"dump", "dump2",
	"vector",
	"VoxelManip", "VoxelArea",
	"PseudoRandom", "PcgRandom",
	"ItemStack",
	"Settings",
	"unpack",
	"sfinv",
	"default",

	table = {
		fields = {
			"copy",
			"indexof",
			"insert_all",
			"key_value_swap",
		}
	},
	string = {
		fields = {
			"split",
			"trim",
		}
	},
	math = {
		fields = {
			"hypot",
			"sign",
			"factorial"
		}
	},
}

globals = {
	"conquer",
	minetest = { fields = { "item_drop" } },
	sfinv = { fields = { "get_homepage_name" } },
}
